from projects.views import P_ListView, P_DetailView, P_CreateView
from django.urls import path


urlpatterns = [
    path("", P_ListView.as_view(), name="list_projects"),
    path("<int:pk>/", P_DetailView.as_view(), name="show_project"),
    path("create/", P_CreateView.as_view(), name="create_project"),
]
