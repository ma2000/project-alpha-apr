from .models import Project
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# Create your views here.


class P_ListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "p_list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class P_DetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "p_detail.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class P_CreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "create.html"
    fields = ["name", "description", "members"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.pk])
