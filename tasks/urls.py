from django.urls import path
from tasks.views import Task_CreateView, Task_ListView, Task_UpdateView

urlpatterns = [
    path("create/", Task_CreateView.as_view(), name="create_task"),
    path("mine/", Task_ListView.as_view(), name="show_my_tasks"),
    path(
        "<int:pk>/complete/", Task_UpdateView.as_view(), name="complete_task"
    ),
]
