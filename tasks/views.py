from django.views.generic import CreateView, ListView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from .models import Task

# Create your views here.


class Task_CreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "createTask.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.project.pk])

    # def form_valid(self, form):
    #     task = form.save(commit=False)
    #     task.assignee = self.request.user
    #     task.save()
    #     return redirect("show_project", pk=task.project.id)


class Task_ListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "list_view.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class Task_UpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "list_view.html"
    fields = ["is_completed"]
    success_url = "/tasks/mine/"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)
